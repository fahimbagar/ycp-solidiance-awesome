module awesomeProject

require (
	github.com/CloudyKit/fastprinter v0.0.0-20170127035650-74b38d55f37a // indirect
	github.com/CloudyKit/jet v2.1.2+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-redis/redis v6.15.2+incompatible
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190712062909-fae7ac547cb7 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190723021737-8bb11ff117ca // indirect
)
