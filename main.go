package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/CloudyKit/jet"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

var views = jet.NewHTMLSet("./views")

type GeoLocation struct {
	Country   string  `json:"country"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

var ASEAN =[]GeoLocation{
	{
		Country:   "Malaysia",
		Latitude:  4.1389087,
		Longitude: 105.1197731,
	},
	{
		Country:   "Cambodia",
		Latitude:  12.1417739,
		Longitude: 102.7359464,
	},
	{
		Country:   "Indonesia",
		Latitude:  -2.3931673,
		Longitude: 108.8383971,
	},
	{
		Country:   "Philippines",
		Latitude:  11.5779262,
		Longitude: 113.5759257,
	},
	{
		Country:   "East Timor",
		Latitude:  -8.7849768,
		Longitude: 124.6076509,
	},
	{
		Country:   "Laos",
		Latitude:  18.1674655,
		Longitude: 99.3618305,
	},
	{
		Country:   "Singapore",
		Latitude:  4.1389087,
		Longitude: 105.1197731,
	},
	{
		Country:   "Vietnam",
		Latitude:  1.3143394,
		Longitude: 103.7041626,
	},
	{
		Country:   "Brunei",
		Latitude:  4.5512832,
		Longitude: 114.1590798,
	},
	{
		Country:   "Burma",
		Latitude:  18.8014967,
		Longitude: 87.6337715,
	},
	{
		Country:   "Thailand",
		Latitude:  4.1389087,
		Longitude: 105.1197731,
	},
}

type Data struct {
	FromCountry string `json:"from_country"`
	FromLocation string `json:"from_location"`
	ToCountry string `json:"to_country"`
	ToLocation string `json:"to_location"`
	Tonnage float64 `json:"tonnage"`
}

type Response struct {
	Success bool `json:"success"`
}

func main() {
	views.SetDevelopmentMode(true)
	r := chi.NewRouter()
	sendCh := make(chan Data)
	shutdownCh := make(chan bool)
	go ConnectRedis(shutdownCh, sendCh)

	srv := &http.Server{
		Addr:         "localhost:5000",
		Handler:      r,
	}

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		view, err := views.GetTemplate("index.jet")
		vars := make(jet.VarMap)
		vars.Set("HelloWorld", "ASEAN Database Development").Set("ASEAN", ASEAN)

		if err != nil {
			w.WriteHeader(503)
			_, _ = fmt.Fprintf(w, "Unexpected error while parsing template: %+v", err.Error())
			return
		}

		var resp bytes.Buffer
		if err = view.Execute(&resp, vars, nil); err != nil {
			w.WriteHeader(503)
			_, _ = fmt.Fprintf(w, "Error when executing template: %+v", err.Error())
			return
		}
		w.WriteHeader(200)
		//_, _ = w.Write(resp.Bytes())
		view.Execute(w, vars, ASEAN)
	})

	r.Post("/add", func(w http.ResponseWriter, r *http.Request) {
		var request Data

		if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
			w.WriteHeader(401)
			_, _ = fmt.Fprintf(w, "Wrong format: %+v", err.Error())
			return
		}

		sendCh<-request

		render.Status(r, 200)
		render.JSON(w, r, request)
	})

	go func() {
		log.Println("Start serving")
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	shutdownCh<-true

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(2))
	defer cancel()

	go func() {
		err := srv.Shutdown(ctx)
		if err != nil {
			log.Panicln("Shutdown error", err)
		}
	}()

	<-ctx.Done()
	log.Println("Shutting down")
}
