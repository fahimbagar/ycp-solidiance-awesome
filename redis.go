package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
	"log"
)

func ConnectRedis(shutdownCh chan bool, sendCh chan Data) {
	client := redis.NewClient(&redis.Options{
		Addr:     "recru.id:6379",
		Password: "0536dfcb9bba697c6bc45c02eb2048ae",
		DB:       0,
	})
	channel := client.Subscribe("awesome")
	defer func() {
		if err := channel.Close(); err != nil {
			log.Fatal(err)
		}
		if err := client.Close(); err != nil {
			log.Fatal(err)
		}
		log.Println("Redis Shutdown")
	}()

	if _, err := client.Ping().Result(); err != nil {
		log.Fatal(err)
	}
	log.Printf("Redis Connected %+v", client.Options())

	go func() {

		for {
			select {
			case data := <-sendCh:
				b, err := json.Marshal(data)
				if err != nil {
					fmt.Println(err)
					return
				}
				client.Publish("awesome", string(b))
			case <-shutdownCh:
				return
			}
		}
	}()
	<-shutdownCh
}
